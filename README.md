# Otus_task3
```
Так как я делал все по инструкции, вначале надо отчистить диск sdb
Удаляем что не нужно

lvremove /dev/otus/test
lvremove /dev/otus/small
vgremove otus
pvremove /dev/sdc
pvremove /dev/sdb
dd if=/dev/zero of=/dev/sdb bs=1M count=10 
hexdump /dev/sda -c | less


Создаем новый раздел чтобы перетащитьна него данные
pvcreate /dev/sdb
vgcreate vg_root /dev/sdb
lvcreate -n lv_root -l +100%FREE /dev/vg_root
mkfs.xfs /dev/vg_root/lv_root
mount /dev/vg_root/lv_root /mnt


Kопируем рабочую систему на sdb
xfsdump -J - /dev/VolGroup00/LogVol00 | xfsrestore -J - /mnt
for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done
chroot /mnt/
grub2-mkconfig -o /boot/grub2/grub.cfg
cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g; s/.img//g"` --force; done
меняем путь к boot в grub - меняем  rd.lvm.lv - можно сделать и при загрузке ос
sed -e ’s|VolGroup00\/LogVol00|vg_root\/lv_root|g’ /boot/grub2/grub.cfg

Вылазием из chroot и перезагружаемся 
chroot /proc/1/cwd/
reboot


Загружаемся с sdb
Уменьшить том под / до 8G
и переносим систему назад
lvremove /dev/VolGroup00/LogVol00
lvcreate -n VolGroup00/LogVol00 -L 8G /dev/VolGroup00
mkfs.xfs /dev/VolGroup00/LogVol00
mount /dev/VolGroup00/LogVol00 /mnt
xfsdump -J - /dev/vg_root/lv_root | xfsrestore -J - /mnt

Поготавливаемся к загрузке с исходного диска sda
for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done
chroot /mnt/
grub2-mkconfig -o /boot/grub2/grub.cfg
cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g; s/.img//g"` --force; done

Выделить том под /var в зеркало
pvcreate /dev/sdc /dev/sdd
vgcreate vg_var /dev/sdc /dev/sdd
lvcreate -L 950M -m1 -n lv_var vg_var
mkfs.ext4 /dev/vg_var/lv_var
mount /dev/vg_var/lv_var /mnt
cp -aR /var/* /mnt/
mkdiir /tmp/oldvar
cp -aR /var/* /tmp/oldvar

монтируем новый var в каталог /var
umount /mnt
mount /dev/vg_var/lv_var /var
Прописываем правила в fstab
echo "`blkid | grep var: | awk '{print $2}'` /var ext4 defaults 0 0" >> /etc/fstab

После чего можно успешно перезагружаться в новый (уменьшенный root) и удалять временную Volume Group:
lvremove /dev/vg_root/lv_root
vgremove /dev/vg_root
pvremove /dev/sdb

Выделить том под /home
lvcreate -n LogVol_Home -L 2G /dev/VolGroup00
mkfs.xfs /dev/VolGroup00/LogVol_Home
mount /dev/VolGroup00/LogVol_Home /mnt/
cp -aR /home/* /mnt/
rm -rf /home/*
mount /dev/VolGroup00/LogVol_Home /home/

Правим fstab для автоматического монтированиā /home
echo "`blkid | grep Home | awk '{print $2}'` /home xfs defaults 0 0" >> /etc/fstab

Сгенерируем файлý в /home/:
touch /home/file{1..20}


Делаем снапшот удаляем данные и откатываем lv к состоянию снапшота для восстановления данных
lvcreate -L 100MB -s -n home_snap /dev/VolGroup00/LogVol_Home
rm -f /home/file{11..20}
Процесс восстановления со снапшота: 
umount /home 
lvconvert --merge /dev/VolGroup00/home_snap 
mount /home


В скрипте выглядит так
[root@lvm ~]# cat shell_record1
Script started on Mon 19 Aug 2019 08:50:20 AM UTC
[root@lvm ~]# ls /home/
file1  file10  file11  file12  file13  file14  file15  file16  file17  file18  file19  file2  file20  file3  file4  file5  file6  file7  file8  file9
[root@lvm ~]#  lvcreate -L 100MB -s -n home_snap /dev/VolGroup00/LogVol_Home
  Rounding up size to full physical extent 128.00 MiB
  Logical volume "home_snap" created.
[root@lvm ~]# lvs
  LV          VG         Attr       LSize   Pool Origin      Data%  Meta%  Move Log Cpy%Sync Convert
  LogVol00    VolGroup00 -wi-ao----   8.00g
  LogVol01    VolGroup00 -wi-ao----   1.50g
  LogVol_Home VolGroup00 owi-aos---   2.00g
  home_snap   VolGroup00 swi-a-s--- 128.00m      LogVol_Home 0.00
  lv_var      vg_var     rwi-aor--- 952.00m                                         100.00
[root@lvm ~]# rm -f /home/file{11..20}
[root@lvm ~]# ls /home/
file1  file10  file2  file3  file4  file5  file6  file7  file8  file9
[root@lvm ~]#  umount /home
[root@lvm ~]#  lvconvert --merge /dev/VolGroup00/home_snap
  Merging of volume VolGroup00/home_snap started.
  VolGroup00/LogVol_Home: Merged: 100.00%
[root@lvm ~]#  mount /home
mount: can't find UUID="e6fc66fb-740d-4cb2-b54e-23efb7e67b23"
[root@lvm ~]# mount /dev/VolGroup00/LogVol_Home /home/
[root@lvm ~]# ls /home/
file1  file10  file11  file12  file13  file14  file15  file16  file17  file18  file19  file2  file20  file3  file4  file5  file6  file7  file8  file9
[root@lvm ~]# lvs
  LV          VG         Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  LogVol00    VolGroup00 -wi-ao----   8.00g
  LogVol01    VolGroup00 -wi-ao----   1.50g
  LogVol_Home VolGroup00 -wi-ao----   2.00g
  lv_var      vg_var     rwi-aor--- 952.00m                                    100.00
[root@lvm ~]# exit
exit

Script done on Mon 19 Aug 2019 08:52:11 AM UTC
[root@lvm ~]#


```



